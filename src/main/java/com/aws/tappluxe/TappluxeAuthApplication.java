package com.aws.tappluxe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TappluxeAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(TappluxeAuthApplication.class, args);
	}
		
}
