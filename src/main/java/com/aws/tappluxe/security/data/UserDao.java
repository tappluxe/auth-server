package com.aws.tappluxe.security.data;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface UserDao extends JpaRepository<User, Long>, QueryDslPredicateExecutor<User> {

}
